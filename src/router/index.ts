import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";

// description des pages
const myRoutes = [
  {
    path: "/about",
    name: "about",
    vue: "Page",
    label: "About"
  },
  {
    path: "/test",
    name: "test",
    vue: "Page",
    label: "Test"
  }
];

myRoutes.push({
  path: "/:catchAll(.*)",
  name: "404",
  vue: "Page",
  label: "404 page not found"
});

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "home",
    component: Home
  }
];

for (const item of myRoutes) {
  routes.push({
    path: item.path,
    name: item.name,
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ `../views/${item.vue}.vue`),
    props(route) {
      return {
        vuename: item.label,
        querys: route.query,
        isPage: item.name !== "404"
      };
    }
  });
}
console.log(routes);
const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;
